//
//  Workout+CoreDataProperties.m
//  crosslog
//
//  Created by Mikhail Rakhmalevich on 27.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Workout+CoreDataProperties.h"

@implementation Workout (CoreDataProperties)

@dynamic backendId;
@dynamic comment;
@dynamic items;
@dynamic training;

@end
