//
//  DataModel.h
//  crosslog
//
//  Created by Mikhail Rakhmalevich on 23.10.15.
//  Copyright © 2015 Mikhail Rakhmalevich. All rights reserved.
//

#import "User.h"
#import "Gym.h"
#import "Training.h"
#import "Workout.h"
#import "WorkoutItem.h"
#import "Exercise.h"